package com.exhibitme.mvc.model;

/**
 * @author dimyriy
 * @date 11/11/15
 */
public class User {
    private long id;
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getId() {

        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        //noinspection SimplifiableIfStatement
        if (getId() != user.getId()) return false;
        return getUsername().equals(user.getUsername());

    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + getUsername().hashCode();
        return result;
    }
}
