package com.exhibitme.mvc.controller;

import com.exhibitme.mvc.model.User;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.logging.Logger;

/**
 * @author dimyriy
 * @date 11/11/15
 */
@Controller
@Component
public class UserController {
    private static final Logger logger = Logger.getLogger(UserController.class.getName());

    @RequestMapping(value = "{name}", method = RequestMethod.GET)
    public
    @ResponseBody
    User getUser(@PathVariable("name") String name) {
        User user = new User();
        user.setUsername(name);
        user.setId(0L);
        logger.info("Got user request for user " + name);
        return user;
    }
}
