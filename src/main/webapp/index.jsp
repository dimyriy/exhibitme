<!DOCTYPE html>
<html>
<head lang="en">
    <title>ExhibitMe</title>
    <link type="text/css" rel="stylesheet" href="resources/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="resources/css/bootstrap-theme.min.css"/>
    <link type="text/css" rel="stylesheet" href="resources/css/custom-bootstrap.css"/>
    <script src="resources/js/jquery-2.1.3.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/custom-bootstrap.js"></script>
    <meta charset="UTF-8">
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand glyphicon glyphicon-headphones" href="#"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home<span class="sr-only">(current)</span></a></li>
                <li><a href="#">Link</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        Dropdown
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another Action</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Something else</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated Link</a></li>
                        <li class="divider"></li>
                        <li><a href="#">One More Separated Link</a></li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="search">
                </div>
                <button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
            </form>
        </div>
    </div>
</nav>
<img src="resources/images/logo.png"/>
</body>
</html>